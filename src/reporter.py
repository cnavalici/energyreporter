#!/usr/bin/env python3
# -*- coding: UTF-8 -*-

# Energy reporter tool

import argparse
import os
import sys

from energyreporter.RecordsManager import RecordsManager
from energyreporter.TariffsReader import TariffsReader
from energyreporter.helpers import ELECTRIC_HIGH, ELECTRIC_LOW, GAS
from energyreporter.DisplayManager import DisplayManager

from energyreporter.CostsCalculator import CostsCalculator

TARIFFS_GAS_FILE = 'data/tariffs_gas.json'
TARIFFS_ELECTRIC_FILE = 'data/tariffs_electric.json'
DB_NAME = 'data/records.db'

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Energy Reporter Tool")

    parser.add_argument("--print-total-start",  help="Start date for the totals.")
    parser.add_argument("--print-total-end", help="End date for the totals.")

    parser.add_argument('--version', action='version', version='%(prog)s 1.0')

    args = parser.parse_args()

    try:
        dir_path = os.path.dirname(os.path.realpath(__file__))
        tariffs_gas_file = os.path.join(dir_path, TARIFFS_GAS_FILE)
        tariffs_electric_file = os.path.join(dir_path, TARIFFS_ELECTRIC_FILE)

        db_file = os.path.join(dir_path, DB_NAME)

        rm = RecordsManager(db_file=db_file)
        tariff_reader = TariffsReader(tariff_electric_file=tariffs_electric_file, tariff_gas_file=tariffs_gas_file)

        cc = CostsCalculator(tariffs_reader=tariff_reader, db_file=db_file)

        dm = DisplayManager(calculator=cc, records_manager=rm)

        if args.print_total_start or args.print_total_end:
            dm.print_saved_totals(args.print_total_start, args.print_total_end)
            sys.exit(0)

        # regular input procedure
        first_time = not rm.has_records()

        if first_time:
            print("No previous records found.")
        else:
            dm.print_saved_last()

        # input validation
        while True:
            records_date = input("Records date (YYYY-MM-DD): ")
            if rm.validate_records_date(records_date):
                break

        while True:
            records_eh = int(input("Index for ELECTRIC HIGH: "))
            if rm.validate_index(records_eh, ELECTRIC_HIGH):
                break

        while True:
            records_el = int(input("Index for ELECTRIC LOW: "))
            if rm.validate_index(records_eh, ELECTRIC_LOW):
                break

        while True:
            records_gas = float(input("Index for GAS: "))
            if rm.validate_index(records_gas, GAS):
                break

        # calculation part
        if not first_time:
            previous_date = rm.get_last_record_date().isoformat()[:10]

            cc.set_interval(start_date=previous_date, end_date=records_date)

            high = [rm.get_last_index(ELECTRIC_HIGH), records_eh]
            low = [rm.get_last_index(ELECTRIC_LOW), records_el]
            cc.set_electric_indexes(high=high, low=low)

            gas = [rm.get_last_index(GAS), records_gas]
            cc.set_gas_indexes(indexes=gas)

            cc.calculate()

            dm.print_calculated_results()

        # saving part
        save_or_not = input("Do you want to save these values? (Y/N) ")
        if save_or_not.upper() == 'Y':
            rm.save(records_date, records_eh, records_el, records_gas)

            if not first_time:
                cc.save()
        else:
            print("Aborted.")
    except Exception as e:
        print("[{}]: {}".format(type(e), e))
        sys.exit(1)


