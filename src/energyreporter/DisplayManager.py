# -*- coding: UTF-8 -*-
from .helpers import DATE_FORMAT, ELECTRIC_HIGH, ELECTRIC_LOW, GAS
from datetime import datetime


class DisplayManager:
    def __init__(self, calculator=None, records_manager=None):
        """
        :param calculator: CostsCalculator
        :param records_manager: RecordsManager
        """
        self._calculator = calculator
        self._rm = records_manager

    def print_calculated_results(self):
        start, end = self._calculator.get_interval()

        eu = self._calculator.get_calculated_electric_per_unit()
        ed = self._calculator.get_calculated_electric_per_day()
        eus = self._calculator.get_electric_usage()
        gu = self._calculator.get_calculated_gas_per_unit()
        gd = self._calculator.get_calculated_gas_per_day()
        gus = self._calculator.get_gas_usage()

        row_tpl_header = "{:>15}|{:>13}|{:>13}|{:>13}|{:>13}|{:>7}|{:>10}|" \
                         "{:>15}|{:>13}|{:>13}|{:>7}|{:>10}|{:>10}"
        row_tpl_result = "{:>15}|{:>13}|{:>13.2f}|{:>13}|{:>13.2f}|{:>7.2f}|{:>10.2f}|" \
                         "{:>15}|{:>13.2f}|{:>13.2f}|{:>7.2f}|{:>10.2f}|{:>10.2f}"

        print("\n RESULTS for the period {} - {} \n".format(start.strftime(DATE_FORMAT), end.strftime(DATE_FORMAT)))
        print(row_tpl_header.format(
            "Electricity", "High(kwh)", "High(total)", "Low(kwh)", "Low(total)", "Day", "Total(e)",
            "Gas", "Gas(m3)", "Gas(total)", "Day", "Total(g)",
            "TOTAL (e+g)")
        )

        total_e = eu[0] + eu[1] + ed
        total_g = gu[0] + gd

        print(row_tpl_result.format("", eus[0], eu[0], eus[1], eu[1], ed, total_e,
                                    "", gus[0], gu[0], gd, total_g,
                                    total_e + total_g))

    def print_saved_totals(self, start_date="", end_date=""):
        """
        Prints out the total for each category as calculated per a specified period of time.
        :param start_date: 
        :param end_date: 
        :return: 
        """
        if not start_date:
            start_date = self._calculator.DEFAULT_DATE_MIN

        if not end_date:
            end_date = datetime.strftime(datetime.today(), DATE_FORMAT)

        results = self._calculator.all_saved_calculations(start_date, end_date)

        eh = el = ed = gs = gd = 0.00

        for result in results:
            eh += result['eh_cost']
            el += result['el_cost']
            ed += result['el_day_tax_cost']
            gs += result['gas_cost']
            gd += result['gas_day_tax_cost']

        row_tpl_header = "{:>12}|{:>12}|{:>15}|{:>15}|{:>15}|{:>15}|{:>15}|{:>15}"
        row_tpl_result = "{:>12}|{:>12}|{:>15.2f}|{:>15.2f}|{:>15.2f}|{:>15.2f}|{:>15.2f}|{:>15.2f}"

        print(row_tpl_header.format("Start date", "End date", "E High", "E Low", "Gas", "E Days", "G Days", "TOTAL"))
        print(row_tpl_result.format(start_date, end_date, eh, el, gs, ed, gd, eh + el + ed + gs + gd))

    def print_saved_last(self):
        row_tpl = "{:>20}|{:>15}|{:>15}|{:>15}\n"

        lrd = datetime.strftime(self._rm.get_last_record_date(), DATE_FORMAT)
        eh = self._rm.get_last_index(ELECTRIC_HIGH)
        el = self._rm.get_last_index(ELECTRIC_LOW)
        gas = self._rm.get_last_index(GAS)

        print(row_tpl.format("Last record date", "Electric High", "Electric Low", "Gas"))
        print(row_tpl.format(lrd, eh, el, gas))

