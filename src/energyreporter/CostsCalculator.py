# -*- coding: UTF-8 -*-
import sqlite3
from datetime import datetime

from .helpers import DATE_FORMAT, ELECTRIC_HIGH, ELECTRIC_LOW, ELECTRIC, GAS, dict_factory


class CostsCalculator:
    ALL = 'ALL'
    LAST = 'LAST'
    DEFAULT_DATE_MIN = '1900-01-01'
    DEFAULT_DATE_MAX = '9999-12-31'

    _tariffs_reader = None

    def __init__(self, db_file=None, tariffs_reader=None):
        """
        :param db_file: string full path of the database sqlite3 file
        :param tariffs_reader: TariffsReader
        """
        self._create_db(db_file)

        self._tariffs_reader = tariffs_reader

        #                energy_type: [ [per unit], per_day ]
        self._results = {ELECTRIC: [[0, 0], 0], GAS: [[0], 0]}

        self._start_date = None
        self._end_date = None

        self._electric_indexes = {ELECTRIC_HIGH: [], ELECTRIC_LOW: []}
        self._gas_indexes = []

    def _create_db(self, db_file):
        """
        Creates the working sqlite3 db
        :param db_file: string - path of the sqlite3 db
        :return: 
        """
        self._conn = sqlite3.connect(db_file)
        self._conn.row_factory = dict_factory

        self._c = self._conn.cursor()

        self._c.execute(
            '''
            CREATE TABLE IF NOT EXISTS energy_costs (
            start_date TEXT, end_date TEXT, 
            eh_usage INTEGER, eh_price REAL, eh_cost REAL,
            el_usage INTEGER, el_price REAL, el_cost REAL,
            gas_usage INTEGER, gas_price REAL, gas_cost REAL,
            el_day_tax REAL, el_day_tax_cost REAL,
            gas_day_tax REAL, gas_day_tax_cost REAL)
            '''
            )

    def set_interval(self, start_date, end_date):
        """
        Set the interval of calculation
        :param start_date: string
        :param end_date: string
        :return: void
        """
        sd = datetime.strptime(start_date, DATE_FORMAT)
        ed = datetime.strptime(end_date, DATE_FORMAT)

        if sd > ed:
            sd, ed = ed, sd

        self._start_date = sd
        self._end_date = ed

    def get_interval(self):
        return [self._start_date, self._end_date]

    def set_electric_indexes(self, high=[0, 0], low=[0, 0]):
        """
        Sets a pair of start/end values for ELECTRIC type, split by high (normaal) tariff and low (dal) tariff
        :param high: list [start_value, end_value]
        :param low: list [start_value, end_value] this should be [0, 0] in case of a single tariff
        :return: void
        """
        if len(high) != 2:
            raise Exception("The expected list for high electrical indexes should have only 2 elements.")

        if len(low) != 2:
            raise Exception("The expected list for low electrical indexes should have only 2 elements.")

        if len([e for e in high if e < 0]) or len([e for e in low if e < 0]):
            raise Exception("One or more elements in the parameters are negative. Only positives allowed.")

        self._electric_indexes[ELECTRIC_HIGH] = high
        self._electric_indexes[ELECTRIC_LOW] = low

    def set_gas_indexes(self, indexes=[0, 0]):
        """
        Sets a pair of start/end values for GAS type.
        :param indexes: list [start_value, end_value]
        :return: void
        """
        if len(indexes) != 2:
            raise Exception("The expected list for gas indexes should have only 2 elements.")

        if len([e for e in indexes if e < 0]):
            raise Exception("One or more elements in the parameters are negative. Only positives allowed.")

        self._gas_indexes = indexes

    def get_electric_usage(self):
        """
        Returns the number of kwh based on provided indexes
        :return: list [high_kwh, low_kwh]
        """
        high = self._electric_indexes[ELECTRIC_HIGH]
        low = self._electric_indexes[ELECTRIC_LOW]

        return [high[1] - high[0], low[1] - low[0]]

    def get_gas_usage(self):
        """
        Returns the number of kwh based on provided indexes
        :return: list [gas_m3]
        """
        return [self._gas_indexes[1] - self._gas_indexes[0]]

    def calculate(self):
        """
        Main calculation
        Some parameters need to be setup in advance, this method includes the checks
        :return: void
        """
        if not self._start_date or not self._end_date:
            raise Exception("Either start or end date is missing. Use set_interval method first.")

        # electric per day
        te_per_day = self._tariffs_reader.get_electric_per_day()
        e_per_day = self._calculate_energy_per_day(self._start_date, self._end_date, te_per_day)
        self._results[ELECTRIC][1] = e_per_day

        # electric per unit
        if self._electric_indexes[ELECTRIC_HIGH]:
            te_per_unit = self._tariffs_reader.get_electric_per_unit()
            e_per_unit = self._calculate_electric_per_unit(self._electric_indexes, te_per_unit)
            self._results[ELECTRIC][0] = e_per_unit

        # gas per day
        tg_per_day = self._tariffs_reader.get_gas_per_day()
        g_per_day = self._calculate_energy_per_day(self._start_date, self._end_date, tg_per_day)
        self._results[GAS][1] = g_per_day

        # gas per unit
        if self._gas_indexes:
            tg_per_unit = self._tariffs_reader.get_gas_per_unit()
            g_per_unit = self._calculate_gas_per_unit(self._gas_indexes, tg_per_unit)
            self._results[GAS][0] = g_per_unit

    def get_calculated_electric_per_unit(self):
        """
        :return: list [high, low] 
        """
        return self._results[ELECTRIC][0]

    def get_calculated_electric_per_day(self):
        """
        :return: float 
        """
        return self._results[ELECTRIC][1]

    def get_calculated_gas_per_unit(self):
        """
        :return: list [single] 
        """
        return self._results[GAS][0]

    def get_calculated_gas_per_day(self):
        """
        :return: float 
        """
        return self._results[GAS][1]

    def _calculate_electric_per_unit(self, indexes, tariffs):
        """
        Internal custom method to calculate costs for electricity
        :param indexes: dictionary as self._electric_indexes
        :param tariffs: list [high, low]
        :return: list [costs_unit_high, costs_unit_low]
        """
        costs_unit_high = round((indexes[ELECTRIC_HIGH][1] - indexes[ELECTRIC_HIGH][0]) * tariffs[0], 4)
        costs_unit_low = round((indexes[ELECTRIC_LOW][1] - indexes[ELECTRIC_LOW][0]) * tariffs[1], 4)

        return [costs_unit_high, costs_unit_low]

    def _calculate_gas_per_unit(self, indexes, tariffs):
        """
        Internal custom method to calculate costs for gas
        :param indexes: list [start, end]
        :param tariffs: list [high, low]
        :return: list [costs_unit]
        """
        costs_unit = round((indexes[1] - indexes[0]) * tariffs[0], 4)

        return [costs_unit]

    def _calculate_energy_per_day(self, sdate, edate, tariff):
        """
        Common function to calculate the price based on the daily tariff
        :param sdate: datetime
        :param edate: datetime 
        :param tariff: float
        :return: float
        """
        diff = edate - sdate

        price = round((diff.days + 1) * tariff, 2)

        return price

    def save(self):
        """
        Saves the calculation previously done
        :return: bool
        """
        if not self._start_date or not self._end_date:
            return False

        stmt = '''
            INSERT INTO energy_costs VALUES (:start_date, :end_date, 
            :eh_usage, :eh_price, :eh_cost,
            :el_usage, :el_price, :el_cost,
            :gas_usage, :gas_price, :gas_cost,
            :el_day_tax, :el_day_tax_cost,
            :gas_day_tax, :gas_day_tax_cost)
            '''
        tr = self._tariffs_reader

        data = {
            'start_date': self._start_date.strftime(DATE_FORMAT), 'end_date': self._end_date.strftime(DATE_FORMAT),
            'eh_usage': self.get_electric_usage()[0],
            'eh_price': tr.get_electric_per_unit()[0],
            'eh_cost': self.get_calculated_electric_per_unit()[0],
            'el_usage': self.get_electric_usage()[1],
            'el_price': tr.get_electric_per_unit()[1],
            'el_cost': self.get_calculated_electric_per_unit()[1],
            'gas_usage': self.get_gas_usage()[0],
            'gas_price': tr.get_gas_per_unit()[0],
            'gas_cost': self.get_calculated_gas_per_unit()[0],
            'el_day_tax': tr.get_electric_per_day(),
            'el_day_tax_cost': self.get_calculated_electric_per_day(),
            'gas_day_tax': tr.get_gas_per_day(),
            'gas_day_tax_cost': self.get_calculated_gas_per_day()
        }

        # print(data)
        self._c.execute(stmt, data)
        self._conn.commit()

        return True

    def last_saved_calculation(self):
        """
        Retrives last saved calculation in the db
        :return: dict
        """
        self._c.execute("SELECT * FROM energy_costs ORDER BY rowid DESC LIMIT 1")
        return self._c.fetchone()

    def all_saved_calculations(self, start=DEFAULT_DATE_MIN, end=DEFAULT_DATE_MAX):
        """
        Retrieves all saved calculations (using an interval if parameters provided)
        :param start: string DATE_FORMAT 
        :param end: string   DATE_FORMAT
        :return: list of dict
        """
        stmt = "SELECT * FROM energy_costs WHERE start_date >= :startdate AND end_date <= :enddate"
        self._c.execute(stmt, {'startdate': start, 'enddate': end})
        return self._c.fetchall()
