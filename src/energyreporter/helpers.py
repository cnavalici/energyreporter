# -*- coding: UTF-8 -*-
GAS = 'GAS'
ELECTRIC = 'ELECTRIC'
ELECTRIC_HIGH = 'ELECTRIC_HIGH'
ELECTRIC_LOW = 'ELECTRIC_LOW'
DATE_FORMAT = '%Y-%m-%d'


# Sqlite3 dictionary factory to return rows as named records (keys => value)
def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d
