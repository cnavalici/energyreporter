# -*- coding: UTF-8 -*-

import sqlite3
from datetime import datetime, MINYEAR
from .helpers import DATE_FORMAT, ELECTRIC_LOW, ELECTRIC_HIGH, GAS


class RecordsManager:
    conn = None     # sqlite connection
    c = None        # sqlite cursor

    default_date = datetime(MINYEAR, 1, 1)

    def __init__(self, db_file=None):
        self.conn = sqlite3.connect(db_file)
        self.c = self.conn.cursor()

        self.c.execute('''CREATE TABLE IF NOT EXISTS 
            energy_data (date TEXT, electric_high INTEGER, electric_low INTEGER, gas INTEGER)'''
        )

    def get_last_record_date(self):
        """
        Gets the last recorded date. If not found, a default value is returned
        :return: datetime
        """
        try:
            date, _, _, _ = self._get_last_records()
            return datetime.strptime(date, DATE_FORMAT)
        except TypeError:
            return self.default_date

    def has_records(self):
        """
        Checks if there is any record at all
        :return: bool
        """
        return self.get_last_record_date() != self.default_date

    def get_last_index(self, index_type):
        """
        Gets the last value for a specific type of energy
        :param index_type: one of the constants (ELECTRIC_HIGH, ELECTRIC_LOW, GAS)
        :return: integer
        """
        if index_type not in (ELECTRIC_LOW, ELECTRIC_HIGH, GAS):
            raise Exception("Unknown index type {}".format(index_type))

        try:
            _, eh, el, gas = self._get_last_records()

            if index_type == ELECTRIC_HIGH:
                return eh

            if index_type == ELECTRIC_LOW:
                return el

            if index_type == GAS:
                return gas
        except TypeError:
            return 0

    def save(self, rdate, eh, el, gas):
        """
        Saves the records
        :param rdate: string
        :param eh: integer
        :param el: integer
        :param gas: integer
        :return: void
        """
        stmt = "INSERT INTO energy_data VALUES (?, ?, ?, ?)"
        self.c.execute(stmt, (rdate, eh, el, gas))
        self.conn.commit()

    def validate_records_date(self, records_date):
        """
        Validates the input record date
        :param records_date: string as in DATE_FORMAT
        :return: bool
        """
        try:
            records_date = datetime.strptime(records_date, DATE_FORMAT)
            last_date = self.get_last_record_date()

            return records_date > last_date
        except (ValueError, TypeError):
            return False

    def validate_index(self, index_value, index_type):
        """
        Validates a specific type of index
        :param index_value: float
        :param index_type: one of the constants (ELECTRIC_HIGH, ELECTRIC_LOW, GAS)
        :return: bool
        """
        index_value = float(index_value)
        comparison_value = self.get_last_index(index_type)

        return index_value >= comparison_value

    def _get_last_records(self):
        """Gets the last records saved into db"""
        self.c.execute("SELECT * FROM energy_data ORDER BY date DESC LIMIT 1")
        return self.c.fetchone()