# -*- coding: UTF-8 -*-

import json
from .helpers import GAS, ELECTRIC


class TariffsReader:
    # these are the elements from tariffs json files
    DATA_KEY = '_data'
    DATA_COMMON_NAMES = ('DELIVERYDAY', 'NETWORKDAY', 'TAX1UNIT', 'TAX2UNIT')
    DATA_ELECTRIC_NAMES = ('COST1KWH', 'COST2KWH', 'TAXDAY')
    DATA_GAS_NAMES = ('COSTM3',)

    _tariffs_data = {
        ELECTRIC: [],
        GAS: []
    }

    def __init__(self, tariff_electric_file="", tariff_gas_file=""):
        with open(tariff_electric_file) as file:
            tariffs_data = json.load(file)
            self._validate_tariffs(tariffs_data, ELECTRIC)

        with open(tariff_gas_file) as file:
            tariffs_data = json.load(file)
            self._validate_tariffs(tariffs_data, GAS)

    def _validate_tariffs(self, tariffs_data, tariffs_type):
        try:
            all_data = tariffs_data[self.DATA_KEY]
        except KeyError as e:
            raise Exception("Incorrect tariffs data. Missing top level element {}.".format(self.DATA_KEY))

        data_names = [data['_name'] for data in all_data if True]
        specific_names = self.DATA_GAS_NAMES if tariffs_type == GAS else self.DATA_ELECTRIC_NAMES

        for dn in self.DATA_COMMON_NAMES + specific_names:
            if dn not in data_names:
                raise Exception("Incorrect tariffs data. Missing data name {}.".format(dn))

        self._tariffs_data[tariffs_type] = all_data

    def get_electric_per_unit(self):
        """
        Gets the total price of the electricity unit (kwh)
        :return: list [high_tariff, low_tariff]
        """
        high = [t['_value'] for t in self._tariffs_data[ELECTRIC]
                if t['_name'] in ('COST1KWH', 'TAX1UNIT', 'TAX2UNIT')]
        high = round(sum(high), 4)

        low = [t['_value'] for t in self._tariffs_data[ELECTRIC]
               if t['_name'] in ('COST2KWH', 'TAX1UNIT', 'TAX2UNIT')]
        low = round(sum(low), 4)

        return [high, low]

    def get_gas_per_unit(self):
        """
        Gets the total price of the gas unit (m3)
        :return: list [single_tariff]
        """
        single = [t['_value'] for t in self._tariffs_data[GAS]
                  if t['_name'] in ('COSTM3', 'TAX1UNIT', 'TAX2UNIT')]
        single = round(sum(single), 4)

        return [single]

    def get_electric_per_day(self):
        """
        Gets the daily price for electricity
        :return: float
        """
        price = [t['_value'] for t in self._tariffs_data[ELECTRIC]
                 if t['_name'] in ('DELIVERYDAY', 'NETWORKDAY', 'TAXDAY')]
        price = round(sum(price), 4)

        return price

    def get_gas_per_day(self):
        """
        Gets the daily price for gas
        :return: float
        """
        price = [t['_value'] for t in self._tariffs_data[GAS]
                 if t['_name'] in ('DELIVERYDAY', 'NETWORKDAY')]
        price = round(sum(price), 4)

        return price
