# -*- coding: UTF-8 -*-
# RUN IT AS
#       python setup.py pytest
# OR
#       PYTHONPATH=. pytest
#       PYTHONPATH=. pytest --cov=src.energyreporter

import pytest
import os.path

from src.energyreporter.TariffsReader import TariffsReader

DATA_FOLDER = 'data'

TARIFFS_ELECTRIC_FILE = 'tariffs_electric.json'
TARIFFS_GAS_FILE = 'tariffs_gas.json'
INVALID_TARIFFS_ELECTRIC_FILE = 'invalid_tariffs_electric.json'
INVALID_TARIFFS_GAS_FILE = 'invalid_tariffs_gas.json'


class TestTariffsReader:
    @classmethod
    def setup_class(cls):
        dir_path = os.path.dirname(os.path.realpath(__file__))
        data_path = os.path.join(dir_path, DATA_FOLDER)

        cls.tariffs_gas_file = os.path.join(data_path, TARIFFS_GAS_FILE)
        cls.tariffs_electric_file = os.path.join(data_path, TARIFFS_ELECTRIC_FILE)
        cls.i_tariffs_gas_file = os.path.join(data_path, INVALID_TARIFFS_GAS_FILE)
        cls.i_tariffs_electric_file = os.path.join(data_path, INVALID_TARIFFS_ELECTRIC_FILE)

        cls.treader = TariffsReader(tariff_electric_file=cls.tariffs_electric_file,
                                    tariff_gas_file=cls.tariffs_gas_file)

    def test_with_nonexistent_tariffs_files(self):
        with pytest.raises(Exception) as excinfo:
            TariffsReader(tariff_electric_file="invalid_file", tariff_gas_file="invalid_file")

    def test_with_invalid_tariffs_files_missing_top_element(self):
        with pytest.raises(Exception) as excinfo:
            TariffsReader(tariff_electric_file=self.tariffs_electric_file, tariff_gas_file=self.i_tariffs_gas_file)

        assert "Missing top level element {}".format(TariffsReader.DATA_KEY) in str(excinfo.value)

    def test_with_invalid_tariffs_files_missing_elements(self):
        with pytest.raises(Exception) as excinfo:
            TariffsReader(tariff_electric_file=self.i_tariffs_electric_file, tariff_gas_file=self.tariffs_gas_file)

        assert "Incorrect tariffs data. Missing data name" in str(excinfo.value)

    def test_get_electric_per_unit(self):
        assert [0.22, 0.21] == self.treader.get_electric_per_unit()

    def test_get_electric_per_day(self):
        assert -0.05 == self.treader.get_electric_per_day()

    def test_get_gas_per_unit(self):
        assert [0.65] == self.treader.get_gas_per_unit()

    def test_get_gas_per_day(self):
        assert 0.75 == self.treader.get_gas_per_day()