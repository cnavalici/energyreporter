# -*- coding: UTF-8 -*-
# RUN IT AS
#       python setup.py pytest
# OR
#       PYTHONPATH=. pytest
#       PYTHONPATH=. pytest --cov=src.energyreporter --cov-report=html

import pytest
from unittest.mock import patch

from src.energyreporter.CostsCalculator import CostsCalculator
from src.energyreporter.helpers import DATE_FORMAT

SQL_TEST_FILE = 'test.db'


@pytest.fixture
def tariffs_reader():
    with patch('src.energyreporter.TariffsReader.TariffsReader') as mock:
        tariff_mock = mock()

        tariff_mock.get_electric_per_unit.return_value = [0.25, 0.30]
        tariff_mock.get_gas_per_unit.return_value = [0.50]
        tariff_mock.get_electric_per_day.return_value = 1
        tariff_mock.get_gas_per_day.return_value = 2

    return tariff_mock

@pytest.fixture
def calculator(tmpdir, tariffs_reader):
    """Creates a CostsCalculator with some seed data"""
    calc = CostsCalculator(db_file=str(tmpdir.join(SQL_TEST_FILE)), tariffs_reader=tariffs_reader)

    return calc


class TestCostsCalculator:
    def test_set_interval_ordered_dates(self, calculator):
        calculator.set_interval('2017-01-01', '2018-12-31')
        sd, ed = calculator.get_interval()
        assert sd.strftime(DATE_FORMAT) == '2017-01-01'
        assert ed.strftime(DATE_FORMAT) == '2018-12-31'

    def test_set_interval_unordered_dates(self, calculator):
        calculator.set_interval('2018-12-31', '2017-01-01')
        assert calculator._start_date.strftime(DATE_FORMAT) == '2017-01-01'
        assert calculator._end_date.strftime(DATE_FORMAT) == '2018-12-31'

    def test_set_electrical_indexes_only_high_ok(self, calculator):
        with pytest.raises(Exception) as excinfo:
            calculator.set_electric_indexes([1, 2], [1, 2, 3])

        assert "The expected list for low electrical indexes should have only 2 elements" in str(excinfo.value)

    def test_set_electrical_indexes_only_low_ok(self, calculator):
        with pytest.raises(Exception) as excinfo:
            calculator.set_electric_indexes([1, 2, 3], [1, 2])

        assert "The expected list for high electrical indexes should have only 2 elements" in str(excinfo.value)

    def test_set_electrical_indexes_negative_high(self, calculator):
        with pytest.raises(Exception) as excinfo:
            calculator.set_electric_indexes([1, -2], [1, 2])

        assert "Only positives allowed" in str(excinfo.value)

    def test_set_electrical_indexes_negative_low(self, calculator):
        with pytest.raises(Exception) as excinfo:
            calculator.set_electric_indexes([1, 2], [-1, -2])

        assert "Only positives allowed" in str(excinfo.value)

    def test_set_electrical_indexes_ok(self, calculator):
        calculator.set_electric_indexes([1, 2], [3, 6])
        el_usage = calculator.get_electric_usage()

        assert el_usage == [1, 3]

    def test_set_gas_indexes_not_ok(self, calculator):
        with pytest.raises(Exception) as excinfo:
            calculator.set_gas_indexes([0, 0, 1, 2])

        assert "The expected list for gas indexes should have only 2 elements" in str(excinfo.value)

    def test_set_gas_indexes_negative(self, calculator):
        with pytest.raises(Exception) as excinfo:
            calculator.set_gas_indexes([-1, 2])

        assert "Only positives allowed" in str(excinfo.value)

    def test_set_gas_indexes_ok(self, calculator):
        calculator.set_gas_indexes([1, 10])
        gas_usage = calculator.get_gas_usage()

        assert gas_usage == [9]

    def test_calculate_no_interval(self, calculator):
        with pytest.raises(Exception) as excinfo:
            calculator.calculate()

        assert "Either start or end date is missing" in str(excinfo.value)

    def test_calculate_no_indexes(self, calculator):
        calculator.set_interval('2018-01-01', '2018-01-10')
        calculator.calculate()

        # default values are return based on indexes = 0 but days are calculated
        assert calculator.get_calculated_electric_per_day() == 10     # 1 * 10
        assert calculator.get_calculated_gas_per_day() == 20          # 2 * 10

        assert calculator.get_calculated_electric_per_unit() == [0, 0]
        assert calculator.get_calculated_gas_per_unit() == [0]

    def test_calculate_with_indexes(self, calculator):
        calculator.set_interval('2018-01-01', '2018-01-10')
        calculator.set_electric_indexes([10, 14], [20, 27])
        calculator.set_gas_indexes([30, 45])
        calculator.calculate()

        assert calculator.get_calculated_electric_per_day() == 10   # 1 * 10
        assert calculator.get_calculated_gas_per_day() == 20        # 2 * 10

        assert calculator.get_calculated_electric_per_unit() == [1, 2.1]        # 0.25 * 4 , 0.30 * 7
        assert calculator.get_calculated_gas_per_unit() == [7.5]                # 0.50 * 15

    def test_save(self, calculator):
        calculator.set_interval('2018-01-01', '2018-01-10')
        calculator.set_electric_indexes([10, 14], [20, 27])
        calculator.set_gas_indexes([30, 45])
        calculator.calculate()

        calculator.save()

        last_saved = calculator.last_saved_calculation()
        all_saved = calculator.all_saved_calculations()

        assert last_saved == all_saved[0]  # because we have only one record so far
        expected = {
            'start_date': '2018-01-01', 'end_date': '2018-01-10', 'eh_usage': 4, 'eh_price': 0.25, 'eh_cost': 1.0,
            'el_usage': 7, 'el_price': 0.3, 'el_cost': 2.1, 'gas_usage': 15, 'gas_price': 0.5, 'gas_cost': 7.5,
            'el_day_tax': 1.0, 'el_day_tax_cost': 10.0, 'gas_day_tax': 2.0, 'gas_day_tax_cost': 20.0
        }
        assert last_saved == expected

    def test_save_invalid_op(self, calculator):
        assert calculator.save() is False