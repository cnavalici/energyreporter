# -*- coding: UTF-8 -*-
# RUN IT AS
#       python setup.py pytest
# OR
#       PYTHONPATH=. pytest
#       PYTHONPATH=. pytest --cov=src.energyreporter --cov-report=html

import pytest

from datetime import datetime
from src.energyreporter.RecordsManager import RecordsManager
from src.energyreporter.helpers import ELECTRIC_HIGH, ELECTRIC_LOW, GAS, DATE_FORMAT

SQL_TEST_FILE = 'test.db'


@pytest.fixture
def records_manager(tmpdir):
    """Creates a RecordsManager with some seed data"""
    recman = RecordsManager(db_file=str(tmpdir.join(SQL_TEST_FILE)))
    recman.save('2018-08-01', 1, 1, 1)
    recman.save('2018-08-05', 10, 15, 5)
    recman.save('2018-08-10', 12, 25, 10)
    recman.save('2018-08-15', 20, 33, 20)   # last date
    return recman


class TestRecordsManager:
    def test_get_last_record_date_no_records(self, tmpdir):
        recman = RecordsManager(db_file=str(tmpdir.join(SQL_TEST_FILE)))
        last_record = recman.get_last_record_date()

        assert last_record == recman.default_date

    def test_get_last_record_date(self, records_manager):
        last_date = records_manager.get_last_record_date()
        assert datetime.strftime(last_date, DATE_FORMAT) == '2018-08-15'

    def test_has_records_with_no_records(self, tmpdir):
        recman = RecordsManager(db_file=str(tmpdir.join(SQL_TEST_FILE)))
        has_records = recman.has_records()

        assert has_records is False

    def test_get_last_index_unknown_type(self, tmpdir):
        with pytest.raises(Exception) as excinfo:
            recman = RecordsManager(db_file=str(tmpdir.join(SQL_TEST_FILE)))
            recman.get_last_index('INDEXUNKNOWN')

        assert "Unknown index type INDEXUNKNOWN" in str(excinfo.value)

    def test_get_last_index_known_types(self, records_manager):
        eh_index = records_manager.get_last_index(ELECTRIC_HIGH)
        el_index = records_manager.get_last_index(ELECTRIC_LOW)
        gas_index = records_manager.get_last_index(GAS)

        assert eh_index == 20
        assert el_index == 33
        assert gas_index == 20

    def test_validate_records_date_invalid(self, tmpdir):
        recman = RecordsManager(db_file=str(tmpdir.join(SQL_TEST_FILE)))
        invalid_dates = (
            '12021', '2088-01-111', '1999', '2201-2212', 123
        )
        for invalid_date in invalid_dates:
            assert recman.validate_records_date(invalid_date) is False

    def test_validate_records_date_good(self, records_manager):
        assert records_manager.validate_records_date('2018-08-14') is False
        assert records_manager.validate_records_date('2018-08-15') is False
        assert records_manager.validate_records_date('2018-08-25') is True

    def test_validate_index_unknown_type(self, tmpdir):
        with pytest.raises(Exception) as excinfo:
            recman = RecordsManager(db_file=str(tmpdir.join(SQL_TEST_FILE)))
            recman.validate_index(10, "UNKNOWN")

    def test_validate_index_no_previous_data(self, tmpdir):
        recman = RecordsManager(db_file=str(tmpdir.join(SQL_TEST_FILE)))
        for indexes in (ELECTRIC_HIGH, ELECTRIC_LOW, GAS):
            assert recman.validate_index(10, ELECTRIC_HIGH) is True

